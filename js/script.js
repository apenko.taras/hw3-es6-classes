class Employee {

    constructor(options) {
        this.name = options.name
        this.age = options.age
        this.salary  = options.salary
    }

    get getName() {
        return this.name;
    }

    set setName(value) {
        this.name = value;
    }

    get getAge() {
        return this.age;
    }

    set setAge(value) {
        this.age = value;
    }

    get getSalary() {
        return this.salary;
    }

    set setSalary(value) {
        this.salary = value;
    }
}

class Programmer extends Employee{

    constructor(options) {
        super(options)
        this.lang = options.lang
    }

    get getLang() {
        return this.lang;
    }

    set setLang(value) {
        this.lang = value;
    }

    get getSalary() {
        return this.salary * 3;
    }
}

const  vally = new Employee({
    name: 'Val',
    age: 18,
    salary: 50
})

const  valera = new Programmer({
    name: 'Valera',
    age: 28,
    salary: 3000,
    lang: ['english', 'germany']
})

const  kiry = new Programmer({
    name: 'Kiry',
    age: 30,
    salary: 500,
    lang: ['english', 'france']
})

console.log(vally);
console.log(vally.salary);
console.log(vally.getSalary);

console.log(valera);
console.log(valera.salary);
console.log(valera.getSalary);

console.log(kiry);
console.log(kiry.salary);
console.log(kiry.getSalary);